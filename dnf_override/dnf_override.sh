#!/bin/bash

#Goal
#    Override dnf and ym command in a rpm-ostree environment
#    removing all options like unmanaged --skip-broken
#    and adding other option like -A to instantly apply changes ecc.

#Use
#    override dnf command creating an alias like the following
#    alias dnf="source ~/dnf_override.sh"
#
#    To use it over ssh connection It must be included in a .bashrc file as follow:
#    alias dnf="source ~/dnf_override.sh"
#    shopt -s expand_aliases

#NOTE
#    This script log each usage into the <log_file_path> file

command_str=""
skipped=""
log_file_path="/var/tmp/dnf_override.log"

for arg in $*; do
    if grep -E '^-' <<< $arg > /dev/null || [[ $arg == "dnf" ]] || [[ $arg == "yum" ]] || [[ $arg == "install" ]];
    then
        skipped="$skipped $arg"
    else
        command_len=${#command_str}
        if [ $command_len -gt 0 ]; then
            command_str="$command_str $arg"
        else
            command_str="$arg"
        fi
    fi
done

command_str="rpm-ostree install -A --allow-inactive --idempotent $command_str"

if [ ! -f $log_file_path ]; then
  touch $log_file_path
fi

echo "$(date)" >> $log_file_path
echo "command: $command_str" >> $log_file_path
echo "skipped: $skipped" >> $log_file_path
out_value=$($command_str > /dev/null)
echo $out_value
