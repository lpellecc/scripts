# scripts

Collection of scripts that can be usefull in testing activities

## dnf override to install packages with rpm-ostree

### Folder
dnf_override

### Description
Override dnf and ym command in a rpm-ostree environment removing all options like unmanaged **--skip-broken** and adding other option like -A to instantly apply changes ecc.

### Usage
Override dnf command creating an alias like the following
```bash
alias dnf="source ~/dnf_override.sh"
```

To use it over ssh connection It must be included in a **.bashrc** file as follow:
```bash
alias dnf="source ~/dnf_override.sh"
shopt -s expand_aliases
```

###NOTE
This script log each usage into the **log_file_path** file
